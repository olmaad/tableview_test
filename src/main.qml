import QtQuick 2.12
import QtQuick.Window 2.11
import QtQuick.Controls 2.5

Window {
    visible: true

    width: 640
    height: 480

    title: "TableViewApp"

    TableView {
        anchors.fill: parent

        boundsBehavior: Flickable.StopAtBounds

        model: textTableModel

        delegate: Rectangle {
            implicitWidth: contentText.implicitWidth + 12
            implicitHeight: 30

            color: (row % 2 == 0) ? "steelblue" : "white"

            Rectangle {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    right: parent.right
                }

                width: 1

                color: "gray"
            }

            Text {
                id: contentText

                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: 6
                }

                text: model.display
            }
        }

        ScrollBar.vertical: ScrollBar { }
        ScrollBar.horizontal: ScrollBar { }
    }
}
