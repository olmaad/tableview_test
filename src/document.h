#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QString>
#include <QList>
#include <QSharedPointer>

class Document
{
public:
	Document(int documentColumnCount);

	int lineCount() const;
	int columnCount() const;

	QString field(int line, int column) const;

	bool addLine(const QList<QString>& fields);

private:
	int m_columnCount;
	QList<QList<QString>> m_data;

};

typedef QSharedPointer<Document> DocumentPtr;

#endif // DOCUMENT_H