#ifndef TEXTTABLEMODEL_H
#define TEXTTABLEMODEL_H

#include "document.h"
#include <QAbstractTableModel>

class TextTableModel : public QAbstractTableModel
{
	Q_OBJECT;

public:
	explicit TextTableModel(QObject* parent = nullptr);

	virtual int rowCount(const QModelIndex& = QModelIndex()) const override;
	virtual int columnCount(const QModelIndex& = QModelIndex()) const override;

	virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

public slots:
	void resetDocument(const DocumentPtr& document);

private:
	DocumentPtr m_document;

};

#endif // TEXTTABLEMODEL_H