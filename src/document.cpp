#include "document.h"
#include <QDebug>

Document::Document(int documentColumnCount) :
	m_columnCount(documentColumnCount)
{
	if (m_columnCount <= 0) {
		qCritical() << "Column count must be more than zero";

		Q_ASSERT(false);

		throw std::runtime_error("Column count must be more than zero");
	}
}

int Document::lineCount() const
{
	return m_data.size();
}

int Document::columnCount() const
{
	return m_columnCount;
}

QString Document::field(int line, int column) const
{
	if (line < 0 || line >= m_data.size() || column < 0 || column >= m_columnCount) {
		qWarning() << "No such field (" << line << "," << column << ")";

		return{};
	}

	return m_data.at(line).at(column);
}

bool Document::addLine(const QList<QString>& fields)
{
	if (fields.size() != m_columnCount) {
		qCritical() << "Field amount must be equal column count";

		Q_ASSERT(false);

		return false;
	}

	m_data.append(fields);

	return true;
}
