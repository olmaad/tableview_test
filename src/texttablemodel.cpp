#include "texttablemodel.h"
#include <QDebug>

TextTableModel::TextTableModel(QObject* parent) :
	QAbstractTableModel(parent)
{}

int	TextTableModel::rowCount(const QModelIndex&) const
{
	return m_document ? m_document->lineCount() : 0;
}

int TextTableModel::columnCount(const QModelIndex&) const
{
	return m_document ? m_document->columnCount() : 0;
}

QVariant TextTableModel::data(const QModelIndex& index, int role) const
{
	if (!m_document || !index.isValid()) {
		return{};
	}

	if (role == Qt::DisplayRole) {
		return m_document->field(index.row(), index.column());
	}

	return{};
}

void TextTableModel::resetDocument(const DocumentPtr& document)
{
	if (!document) {
		qCritical() << "Document is nullptr";

		Q_ASSERT(false);

		return;
	}

	beginResetModel();

	if (document->lineCount() == 0) {
		qWarning() << "Empty document";
	}

	m_document = document;

	endResetModel();
}
