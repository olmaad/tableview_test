#include "parser.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>

Parser::Parser(QObject* parent) :
	QObject(parent)
{}

void Parser::parse(QString filename)
{
	if (!QFile::exists(filename)) {
		qWarning() << "File" << filename << "does not exists";

		emit failed();

		return;
	}

	QFile source(filename);

	if (!source.open(QIODevice::ReadOnly)) {
		qCritical() << "Can not open file" << filename << "to read";

		emit failed();

		return;
	}

	DocumentPtr document;

	QTextStream stream(&source);

	try {
		while (!stream.atEnd()) {
			auto line = stream.readLine();

			if (line.isEmpty()) {
				continue;
			}

			if (!line.endsWith(';')) {
				throw std::exception();
			}

			auto fields = line.split(';');
			fields.removeLast();

			if (!document) {
				document = DocumentPtr::create(fields.size());
			}

			if (!document->addLine(fields)) {
				throw std::exception();
			}
		}
	}
	catch (...) {
		qCritical() << "Error parsing file";

		emit failed();

		return;
	}

	emit parsed(document);
}
