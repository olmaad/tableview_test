#ifndef PARSER_H
#define PARSER_H

#include "document.h"
#include <QObject>
#include <QIODevice>

class Parser : public QObject
{
	Q_OBJECT;

public:
	explicit Parser(QObject* parent = nullptr);

public slots:
	void parse(QString filename);

signals:
	void parsed(DocumentPtr document);
	void failed();

};

#endif // PARSER_H