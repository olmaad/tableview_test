#include "parser.h"
#include "texttablemodel.h"
#include "document.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QThread>
#include <QDebug>

Q_DECLARE_METATYPE(DocumentPtr);

int main(int argc, char** argv)
{
	qRegisterMetaType<DocumentPtr>();

	if (argc != 2) {
		qInfo() << "Usage: tableview <path to file>";

		return 0;
	}

	QString filename(argv[1]);

	if (!QFile::exists(filename)) {
		qWarning() << "File not found";

		return -1;
	}

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

	TextTableModel model(&app);

	engine.rootContext()->setContextProperty("textTableModel", &model);
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

	if (engine.rootObjects().isEmpty()) {
		qCritical() << "Can not load qml";

		Q_ASSERT(false);

		return -1;
	}

	Parser parser;

	QObject::connect(&parser, &Parser::parsed, &model, &TextTableModel::resetDocument);
	QObject::connect(&parser, &Parser::failed, &app, [&app]() {
		app.exit(-1);
	});

	QThread parserThread(&app);
	parser.moveToThread(&parserThread);

	QObject::connect(&app, &QGuiApplication::aboutToQuit, &parserThread, &QThread::terminate);
	QObject::connect(&parserThread, &QThread::started, &parser, [&parser, &filename]() {
		parser.parse(filename);
	}, Qt::QueuedConnection);

	parserThread.start();

    return app.exec();
}
