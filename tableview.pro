QT += quick
CONFIG += c++11

HEADERS += \
    src/texttablemodel.h \
    src/parser.h \
    src/document.h

SOURCES += \
    src/main.cpp \
    src/texttablemodel.cpp \
    src/parser.cpp \
    src/document.cpp

RESOURCES +=
    src/qml.qrc
